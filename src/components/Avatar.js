import React from 'react';


function Avatar(props) {
    return (
        <div className="avatar">
            {
                src ? 
                    <img src={props.src} alt="Avatar" draggable="false"/> :
                    <i className="fas fa-user icon" />
            }
        </div>
    );
};

Avatar.defaultProps = {
    src: null,
};

export default Avatar;