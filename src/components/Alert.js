import React from 'react';

import Button from '@/components/Button';

function Alert(props) {
    return (
        <div className="alert">
            <i className={`${props.icon} fa-lg icon`}/>
            <h4>{ props.message }</h4>
            <Button className="link icon">
                <i className="fas fa-times fa-2x"/>
            </Button>
        </div>
    );
};

Alert.defaultProps = {
    message: "Message",
    icon: "fas fa-question"
};

export default Alert;