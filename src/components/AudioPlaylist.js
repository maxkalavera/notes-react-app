import React from 'react';

import Button from '@/components/Button';
import Timer from '@/components/Timer';

function AudioPlaylist(props) {
    return (
        <table className="audio-playlist">
            <thead>
                <tr>
                    <th><h3>Recordings</h3></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                {
                    props.items.map((item) => (
                        <tr className={`${item.playing ? "playing" : ""}`} key={item.key}>
                            <td><h5>{item.name}</h5></td>
                            <td className="center">
                                <div className="actions">
                                    <h5>
                                        <Timer milliseconds={item.duration} useHours={false}/>
                                    </h5>
                                    {
                                        item.playing ?
                                            <Button className="transparent" 
                                                onClick={() => props.onPause(item.key)}>
                                                <i className="fas fa-pause fa-2x"/>
                                            </Button> :
                                            <Button className="transparent" 
                                                onClick={() => props.onPlay(item.key)}>
                                                <i className="fas fa-play fa-2x"/>
                                            </Button>
                                    }
                                    <Button className="transparent" 
                                        onClick={() => props.onDelete(item.key)}>
                                        <i className="fas fa-trash fa-2x"/>
                                    </Button>
                                </div>
                            </td>
                        </tr>
                    ))
                }
            </tbody>
            {
                props.loadMore ? (
                    <tfoot>
                        <tr>
                            <td colspan="6" align="center">
                                <Button 
                                    onClick={props.loadMore} 
                                    className="link">
                                    <h4>Load More</h4>
                                </Button>
                            </td>
                        </tr>
                    </tfoot>                             
                ) : null
            }
        </table>
    );
};

AudioPlaylist.defaultProps = {
    items: [
        {
            key: 0,
            name: "faef465866794b1f8ab7aab8596ce6c7.wav",
            duration: 13200,
            playing: false
        },
        {
            key: 1,
            name: "85012a4d0887421d8fbecd574ae0f817.wav",
            duration: 45100,
            playing: false
        },
        {
            key: 2,
            name: "19a2253d0dce4721a2c767ba950bbf16.wav",
            duration: 74300,
            playing: false
        }
    ],
    loadMore: null,
    onPlay: null,
    onPause: null,
    onDelete: null
};

export default AudioPlaylist;