import React, {useCallback} from 'react';

import { v4 as uuidv4 } from 'uuid';

function FileUploader(props) {
    const {onFilesAdded} = props;

    const onDrop = useCallback((event) => {
        event.preventDefault();

        const files = [];
        if (event.dataTransfer.items != null && event.dataTransfer.items.length > 0) {
            for (var i=0; i < event.dataTransfer.items.length; i++) {
                files.push(event.dataTransfer.items[i].getAsFile());
            }
            onFilesAdded(files);
        }
    }, [onFilesAdded]);

    const onDragOver = useCallback((event) => {
        event.preventDefault();        
    }, []);

    const onFileUpload = useCallback((event) => {
        const files = [];
        if (event.target.files.length > 0) {
            for (var i=0; i < event.target.files.length; i++) {
                files.push(event.target.files[i]);
            }
            onFilesAdded(files);
        }
    }, [onFilesAdded]);

    return (
        <form className="file-uploader" onDrop={onDrop} onDragOver={onDragOver}>
            <i className="fas fa-cloud-upload-alt fa-10x"/>
            <h3>Drag your file here</h3>
            <label htmlFor={props.inputID} className="file-input-label">
                <h4>Browse file</h4>
            </label>
            <input id={props.inputID} type="file" multiple onChange={onFileUpload}/>
        </form>
    );
};

FileUploader.defaultProps = {
    onFilesAdded: () => {},
    inputID: uuidv4()
};

export default FileUploader;