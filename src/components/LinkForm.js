import React from 'react';

import Button from '@/components/Button';
import Input from '@/components/Input';

function LinkForm(props) {
    return (
        <form className="link-form">
            <Input placeholder="Add URL" 
                label={<i className="fas fa-link fa-1x"/>}
                className="grow"
                disabled={props.disabled}/>
            <Button className="grow"
                disabled={props.disabled}>
                <h4>Save</h4>
            </Button>
        </form>
    );
};

LinkForm.defaultProps = {
    onClick: null,
    disabled: false,
    addLink: null
};

export default LinkForm;