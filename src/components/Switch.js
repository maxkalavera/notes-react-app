import React from 'react';

function Switch(props) {
    return (
        <label className={`switch ${props.className}`}>
            <input
                {...{
                    ...props,
                    className: "",
                    type: "checkbox",
                }} />
            <span className="slider"></span>
        </label>
    );
};

Switch.defaultProps = {
    checked: false,
    onChange: null,
    className: "",
};

export default Switch;