import React, {useState, useEffect, useCallback} from 'react';

import { v4 as uuidv4 } from 'uuid';

import Button from '@/components/Button';

function AudioPlayerTimeline(props) {
    let [seeking, setSeeking] = useState(false);
    const {timelineComponentID, duration, onSeeking, disabled} = props;
    const percentage = (duration > 0) ? parseInt((props.currentTime / duration) * 100) : 0;

    const onMouseDown = useCallback(() => {
        setSeeking(true);
    }, [setSeeking]);

    const onMouseUp = useCallback(() => {
        setSeeking(false);
    }, [setSeeking]);

    const onMouseMove = useCallback((event) => {
        // Calculate selected seconds on video when selecting a time in the timeline bar
        let timeline = document.getElementById(timelineComponentID);

        // The SVG has margins on the timeline they have to be removed to calculate
        let timelineMarginUnit = (timeline.clientWidth / 110.0);
        let timelineStart = timeline.offsetLeft + (timelineMarginUnit * 2);
        let timelineWidth = timeline.clientWidth - (timelineMarginUnit * 4);

        // Set difference between mouse position and begining of timeline
        // Allow only number between 0 and tinline width
        let diff = event.clientX - timelineStart;
        diff = diff > 0 ? diff : 0;
        diff = diff <= timelineWidth ? diff : timelineWidth;
        let percentage = (diff / timelineWidth);
        let seconds = percentage * duration;
    
        onSeeking(seconds);
    }, [timelineComponentID, duration, onSeeking]);

    useEffect(() => {
        if (!disabled) {
            if (seeking) {
                window.onmouseup = onMouseUp;
                window.onmousemove = onMouseMove;
                window.onclick = onMouseMove;
            } else {
                window.onmouseup = null;
                window.onmousemove = null;
            }    
        }
    }, [disabled, seeking, onMouseMove, onMouseUp]);

    return (
        <div className={`timeline ${disabled ? "disabled" : ""}`}>
            <h4>
                {
                    new Intl.DateTimeFormat(
                        process.env.LOCALE,
                        { minute: 'numeric', second: 'numeric'}
                    ).format(new Date(props.currentTime * 1000))
                }
            </h4>
            <div id={timelineComponentID}>
                <svg viewBox="0 0 110 10"
                    xmlns="http://www.w3.org/2000/svg"
                    onMouseDown={disabled ? null : onMouseDown}>
                    <g fill="none" strokeWidth="5">
                        <path className="background-line" strokeLinecap="round" d="M5 5 l100 0"/>
                        <path className="foreground-line" strokeLinecap="round" d={`M5 5 l${percentage} 0`}/>
                    </g>
                    <circle className="pin" cx={`${5 + percentage}`} cy="5" r="5"/>
                    Sorry, your browser does not support inline SVG.
                </svg>
            </div>
            <h4>
                {
                    new Intl.DateTimeFormat(
                        process.env.LOCALE, 
                        { minute: 'numeric', second: 'numeric'}    
                    ).format(new Date(duration * 1000))
                }
            </h4>
        </div>
    );
}

AudioPlayerTimeline.defaultProps = {
    currentTime: 0,
    duration: 0,
    onSeeking: () => {},
    timelineComponentID: uuidv4(),
    disabled: false
};

function VolumeControler(props) {
    const {muted, onMutedChange, volume, onVolumeChange, inputID, disabled} = props;

    const onChange = useCallback((event) => {
        onVolumeChange(event.target.value / 100);
    }, [onVolumeChange]);

    const onMutedButtonClicked = useCallback(() => {
        onMutedChange(!muted);
    }, [muted, onMutedChange]);

    useEffect(() => {
        if (muted) {
            const input = document.getElementById(inputID);
            input.style.backgroundSize = `0% 100%`;
            input.value = 0;
        } else {
            document.getElementById(inputID)
                .style.backgroundSize = `${volume * 100}% 100%`;
        }
    }, [muted, inputID, volume]);

    return (
        <div className="volume-controler">
            <div className="volume-bar-container">
                <input id={inputID} 
                    className="volume-bar"
                    type="range" 
                    value={volume * 100}
                    min="0" max="100" 
                    onChange={onChange}
                    disabled={disabled}/>
            </div>
            <Button className="transparent"
                onClick={onMutedButtonClicked}
                disabled={disabled}>
                {useCallback(() => {
                    if (volume === 0.0 || muted) {
                        return <i className="fas fa-volume-mute"/>;
                    } else if (volume > 0.5) {
                        return <i className="fas fa-volume-up"/>;
                    } else {
                        return <i className="fas fa-volume-down"/>;
                    }
                }, [volume, muted])()}
                
            </Button>
        </div>
    );
};

VolumeControler.defaultProps = {
    volume: 1.0,
    onVolumeChange: () => {},
    muted: false,
    onMutedChange: () => {},
    inputID: uuidv4(),
    disabled: false
};

let audio = null;

function AudioPlayer(props) {
    let [mode, setMode] = useState("inactive");  // playing | paused | inactive
    let [filename, setFilename] = useState("");
    let [duration, setDuration] = useState(0);
    let [volume, setVolume] = useState(0.0);
    let[currentTime, setCurrentTime] = useState(0);
    let[muted, setMuted] = useState(false);

    const {src, disabled} = props;

    const onPlay = useCallback(() => {
        if (audio.currentTime === audio.duration)
            audio.currentTime = 0.0;

        audio.play();
        setMode("playing");
    }, [setMode]);

    const onPause = useCallback(() => {
        audio.pause();
        setMode("paused");
    }, [setMode]);

    const onSeeking = useCallback((time) => {
        audio.currentTime = time;
    }, []);

    const onMutedChange = useCallback((muted) => {
        audio.muted = muted;
        setMuted(muted);
    }, [setMuted]);

    const onVolumeChange = useCallback((volume) => {
        audio.volume = volume;
        setVolume(volume);
        onMutedChange(false);
    }, [setVolume, onMutedChange]);

    useEffect(() => {
        audio = new Audio(src);
        audio.addEventListener('loadeddata', () => {
            setDuration(audio.duration);
            setVolume(audio.volume);
            setCurrentTime(audio.currentTime);
            setMuted(audio.muted);
            if (audio.currentSrc)
                setFilename(new URL(audio.currentSrc).pathname.split('/').pop());
        });

        audio.addEventListener('timeupdate', () => {
            setCurrentTime(audio.currentTime);
        });

        audio.addEventListener('ended', () => {
            setMode("inactive");
        });
    }, [src]);

    switch(mode) {
        case "playing":
            return (
                <div className="audio-player">
                    <div className="content">
                        <div className="buttons">
                            <Button className="yellow small-button" onClick={null} disabled>
                                <i className="fas fa-step-backward fa-1x"/>
                            </Button>
                            <Button className="yellow medium-button" 
                                onClick={onPause}
                                disabled={disabled}>
                                <i className="fas fa-pause fa-2x"/>
                            </Button>
                            <Button className="yellow small-button" onClick={null} disabled>
                                <i className="fas fa-step-forward fa-1x"/>
                            </Button>
                        </div>
                        <h4>{filename}</h4>
                        <AudioPlayerTimeline
                            currentTime={currentTime} 
                            duration={duration}
                            onSeeking={onSeeking}
                            disabled={disabled}/>
                    </div>

                    <VolumeControler 
                        volume={volume} 
                        onVolumeChange={onVolumeChange} 
                        muted={muted}
                        onMutedChange={onMutedChange}
                        disabled={disabled}/>
                </div>
            );

        default:
            return (
                <div className="audio-player">
                    <div className="content">
                        <div className="buttons">
                            <Button className="yellow small-button" onClick={null} disabled>
                                <i className="fas fa-step-backward fa-1x"/>
                            </Button>
                            <Button className="yellow medium-button" 
                                onClick={onPlay}
                                disabled={disabled}>
                                <i className="fas fa-play fa-2x"/>
                            </Button>
                            <Button className="yellow small-button" onClick={null} disabled>
                                <i className="fas fa-step-forward fa-1x"/>
                            </Button>
                        </div>
                        <h4>{filename}</h4>
                        <AudioPlayerTimeline
                            currentTime={currentTime} 
                            duration={duration}
                            onSeeking={onSeeking}
                            disabled={disabled}/>
                    </div>

                    <VolumeControler 
                        volume={volume} 
                        onVolumeChange={onVolumeChange} 
                        muted={muted}
                        onMutedChange={onMutedChange}
                        disabled={disabled}/>
                </div>
            );
    }
};

AudioPlayer.defaultProps = {
    src: `${process.env.PUBLIC_URL}/audio/test.mp4`,
    disabled: false
};

export default AudioPlayer;