import React from 'react';


function Button(props) {
    return (
        <button
            className={`${props.className}`}
            disabled={props.disabled}
            onClick={props.onClick}>
            {props.children}
        </button>
    );
};

Button.defaultProps = {
    onClick: null,
    disabled: false
};

export default Button;