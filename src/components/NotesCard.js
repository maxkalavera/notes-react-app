import React, {useState} from 'react';

import Button from '@/components/Button';
import Checkbox from '@/components/Checkbox';

import image from '@/assets/images/pexels-lisa-9166412.jpg';
import video from '@/assets/images/pexels-jimmy-chan-2224890.jpg';

function NotesCardContent(props) {
    switch(props.type) {
        case "text": return (
            <div className="content text">
                <p>
                "At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat."
                </p>
            </div>
        );
        case "file": return (
            <div className="content file">
                <Button className="transparent">
                    <i className="fas fa-sticky-note fa-2x"/>
                    <h4>file-name.pdf</h4>
                </Button>
            </div>
        );
        case "image": return (
            <div className="content image">
                <img src={image} alt="The uploaded pic" draggable="false"/>
            </div>
        );
        case "video": return (
            <div className="content video">
                <img src={video} alt="video" draggable="false"/>
            </div>
        );
        case "links": return (
            <div className="content links">
                {
                    props.links.map((item) =>
                        <div className="item" key={item.key}>
                            <i className="fas fa-link fa-1x"/>
                            <a href={item.url}>
                                <h4>{item.url}</h4>
                            </a>
                        </div>
                    )
                }
            </div>
        );
        case "audio": return (
            <div className="content audio">
                <Button className="transparent">
                    <i className="fas fa-microphone fa-2x"/>
                    <h4>Audio record 01</h4>
                </Button>
            </div>
        );
        default: return null;
    }
};

NotesCardContent.defaultProps = {
    name: "",
    type: "links",
    links: [
        { key: "0", url: "https://www.example.com/sdasdasdasdasdasdasdasdasdasdasdasdasdas" },
        { key: "1", url: "https://www.example.org/" },
        { key: "2", url: "https://www.example.mx/" }
    ]
};

function NotesCard(props) {
    const {mode, name, type, links} = props;
    let [selected, onSelectedChange] = useState(false);

    if (mode === "select") {
        return (
            <div className={`notes-card ${selected ? "selected" : ""}`}>
                <div className="header">
                    <Button className="transparent name">
                        <h4>{name}</h4>
                    </Button>
                    <Checkbox checked={selected} onChange={() => onSelectedChange(!selected)}/>
                </div>
                <NotesCardContent 
                    type={type} 
                    links={links}/>
            </div>
        );
    } else {
        return (
            <div className="notes-card">
                <div className="header">
                    <Button className="transparent name">
                        <h4>{name}</h4>
                    </Button>
                    <Button className="transparent icon">
                        <i className="fas fa-ellipsis-h fa-2x"/>
                    </Button>    
                </div>
                <NotesCardContent 
                    type={type} 
                    links={links}/>
            </div>
        );
    
    }
};

NotesCard.defaultProps = {
    mode: "default", // default | select
    name: "",
    type: "text",
};

export default NotesCard;