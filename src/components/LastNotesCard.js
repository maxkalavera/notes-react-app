import React, { useCallback } from 'react';

import Button from '@/components/Button';

function LastNotesCard(props) {
    const {type} = props;

    return (
        <div className="last-notes card">
            <Button className="link"><h4>{props.name}</h4></Button>
            <div className="content">
                {
                    useCallback(() => {
                        switch(type) {
                            case "text": return <i class="fas fa-sticky-note fa-2x"/>;
                            case "file": return <i class="fas fa-file fa-2x"/>;
                            case "image": return <i class="fas fa-image fa-2x"/>;
                            case "video": return <i class="fas fa-video fa-2x"/>;
                            case "links": return <i class="fas fa-link fa-2x"/>;
                            case "audio": return <i class="fas fa-microphone fa-2x"/>;
                            default: return null;
                        }
                    }, [type])()
                }

                <div className="info">
                    <small>
                        {new Intl.DateTimeFormat(process.env.LOCALE, {
                            year: "numeric", month: "numeric", day: "2-digit",
                            hour: 'numeric', minute: 'numeric'
                        }).format(props.date)}
                    </small>
                    <div className="topic">
                        <h5>Topic:</h5>
                        <small>{props.topic}</small>
                    </div>
                </div>
            </div>
        </div>
    );
};

LastNotesCard.defaultProps = {
    name: "",
    type: "",
    date: new Date(),
    topic: ""    
};

export default LastNotesCard;