import React from 'react';

const colors = {
    "label-1": "label-1",
    "label-2": "label-2",
    "label-3": "label-3",
    "label-4": "label-4",
    "label-5": "label-5",
    "label-6": "label-6",
    "label-7": "label-7",
    "label-8": "label-8",
    "label-9": "label-9",
    "label-10": "label-10",
    "label-11": "label-11",
    "label-12": "label-12",
    "label-13": "label-13",
    "label-14": "label-14",    
};

function Ribbon(props) {
    const color = colors[props.color];
    return (
        <i className={`ribbon ${ color ? color : "default" } ${props.className}`}/>
    );
};

Ribbon.defaultProps = {
    className: "",
    color: ""
};

export default Ribbon;