import React, {useState, useEffect, useCallback} from 'react';

import { v4 as uuidv4 } from 'uuid';

import Button from '@/components/Button';
import Switch from '@/components/Switch';
import Timer from '@/components/Timer';

const brightnessFilter = function(context, width, height, adjustment) {
    var imageData = context.getImageData(0, 0, width, height);
    for (let i = 0; i < imageData.data.length; i += 4) {
        imageData.data[i] += adjustment; // Red
        imageData.data[i+1] += adjustment; // Green
        imageData.data[i+2] += adjustment; // Blue
    }

    context.putImageData(imageData, 0, 0);
};

function AudioRecorder(props) {
    let [status, setStatus] = useState("inactive");  // recording | paused | inactive
    let [audioOut, setAudioOut] = useState(false);
    let [timer, setTimer] = useState({
        start: Date.now(),
        milliseconds: 0,
    });
    let [audio, setAudio] = useState({
        context: null,
        recorder: null,
        analyser: null,
        bufferLength: 0,
        chunks: []
    });
    let [canvas, setCanvas] = useState({
        canvas: null,
        context: null
    });

    const {visualizerID, disabled} = props;
    const {recorder, bufferLength, analyser} = audio;

    const start = useCallback(() => {
        if (recorder && recorder.state === "inactive") {
            recorder.start(); //(1000 * 1); // every 1 second(s) return data
            recorder.ondataavailable = (event) => {
                setAudio((previousState) => ({
                    ...previousState,
                    chunks: [...previousState.chunks, event.data]
                }));
            };
            setStatus("recording");
            setTimer((previousState) => ({
                ...previousState,
                start: Date.now(),
            }));
        } else if (recorder  && recorder.state === "paused") {
            recorder.resume();
            setStatus("recording");
            setTimer((previousState) => ({
                ...previousState,
                start: Date.now(),
            }));
        }
    }, [recorder]);

    const stop = useCallback(() => {
        if (recorder && (recorder.state === "recording" || recorder.state === "paused")) {
            recorder.stop();
            setAudio((previousState) => ({
                ...previousState,
                chunks: []
            }));
            setStatus("inactive");
            setTimer({
                start: null,
                milliseconds: 0
            });

            //let audioBlob = new Blob(audioChunks);
        }
    }, [recorder]);

    const pause = useCallback(() => {
        if (recorder && recorder.state === "recording") {
            recorder.pause();
            setStatus("paused");
        }
    }, [recorder]);

    // Timer animation
    useEffect(() => {
        if (status === "recording") {
            let intervalID = setInterval(() => {
                let now = Date.now();
                setTimer((previousState) => ({
                    start: now,
                    milliseconds: previousState.milliseconds + (now - previousState.start)
                }));
            }, 1000);
    
            return () => clearInterval(intervalID);
        }
    }, [status]);

    // Sound waves animation
    useEffect(() => {
        const data = new Float32Array(bufferLength);

        const drawWaves = () => {
            if (canvas.canvas) {
                const {context} = canvas;
                const {width, height} = canvas.canvas;

                brightnessFilter(context, width, height, 255 / 12);
                context.strokeStyle = 
                    (status === "recording") ? "#B18300" : "#1B1B1B";
                context.beginPath();
                context.lineWidth = 2;

                context.moveTo(0, height/2);
                for(let i = 0; i < bufferLength; i++) {
                    context.lineTo(
                        (width * i) / bufferLength, 
                        (height / 2) + (data[i] * 200)
                    );
                }
                context.lineTo(width, height/2);
                context.stroke();
            }
        };

        if (analyser) {
            // Set animation
            const intervalID = setInterval(() => {   
                analyser.getFloatTimeDomainData(data); // Fetch data
                drawWaves();
            }, 1000 / 12);

            return () => clearInterval(intervalID);
        } else {
            drawWaves();
        }
    }, [canvas, status, bufferLength, analyser]);

    // Initial Set up
    useEffect(() => {
        // Canvas initialization
        let canvas = document.getElementById(visualizerID);
        let canvasContext = canvas.getContext("2d");
        setCanvas({
            canvas: canvas,
            context: canvasContext,
        });
    }, [visualizerID]);

    useEffect(() => {
        if (! disabled) {
            navigator.mediaDevices.getUserMedia({ audio: true })
            .then(stream => {
                let mediaRecorder = new MediaRecorder(stream)

                // Initialize microphone audio components and required objects
                let context = new (window.AudioContext || window.webkitAudioContext)();                
                
                let analyser = context.createAnalyser();
                analyser.fftSize = 512;

                let compressor = context.createDynamicsCompressor();
                compressor.threshold.value = -50;
                compressor.knee.value = 40;
                compressor.ratio.value = 12;
                compressor.attack.value = 0;
                compressor.release.value = 0.25;

                let filter = context.createBiquadFilter();
                filter.Q.value = 10;
                filter.frequency.value = 330;
                filter.gain.value = 10.0;
                filter.type = "bandpass";

                // Connect source stream to analizer
                let source = context.createMediaStreamSource(stream);
                source.connect(compressor);
                compressor.connect(filter);
                filter.connect(analyser);
                if (audioOut)
                    analyser.connect(context.destination);

                setAudio((previousState) => {
                    if (previousState.context && previousState.context.state === "running")
                        previousState.context.close();
                    
                    return {
                        ...previousState,
                        context: context,
                        recorder: mediaRecorder,
                        analyser: analyser,
                        bufferLength: analyser.frequencyBinCount,
                    }
                });

                return () => {
                    if (context.state === "running")
                        context.close();
                }
            }).catch((error) => {
                console.log("ERROR", error)
            });
        }
    }, [disabled, audioOut]);

    switch(status) {
        case "recording":
            return (
                <div className="audio-recorder">
                    <div className="audio-out">
                            <i className="fas fa-headphones"/>
                            <Switch className="yellow" 
                                checked={audioOut}
                                onChange={() => setAudioOut(previousState => !previousState)}
                                disabled={props.disabled || !recorder}/>
                    </div>
                    <div className="buttons">
                        <div className="blank"/>
                        <Button className="yellow medium-button"
                            onClick={stop}
                            disabled={props.disabled || !recorder}>
                            <i className="fas fa-stop fa-2x"/>
                        </Button>
                        <Button className="yellow small-button"
                            onClick={pause}
                            disabled={props.disabled || !recorder}>
                            <i className="fas fa-pause fa-1x"/>
                        </Button>
                    </div>
                    <h2><Timer milliseconds={timer.milliseconds}/></h2>
                    <canvas id={props.visualizerID} className="visualizer" width="300" height="150"></canvas>
                </div>
            );
        case "paused":
            return (
                <div className="audio-recorder">
                    <div className="audio-out">
                            <i className="fas fa-headphones"/>
                            <Switch className="yellow"
                                checked={audioOut}
                                onChange={() => setAudioOut(previousState => !previousState)}
                                disabled={props.disabled || !recorder}/>
                    </div>
                    <div className="buttons">
                        <div className="blank"/>
                        <Button className="yellow medium-button" 
                            onClick={start}
                            disabled={props.disabled || !recorder}>
                            <i className="fas fa-microphone fa-2x"/>
                        </Button>
                        <Button className="yellow small-button" 
                            onClick={stop}
                            disabled={props.disabled || !recorder}>
                            <i className="fas fa-stop fa-1x"/>
                        </Button>
                    </div>
                    <h2><Timer milliseconds={timer.milliseconds}/></h2>
                    <canvas id={props.visualizerID} className="visualizer" width="300" height="150"></canvas>
                </div>
                );    
        default:
            return (
                <div className="audio-recorder">
                    <div className="audio-out">
                            <i className="fas fa-headphones"/>
                            <Switch className="yellow"
                                checked={audioOut}
                                onChange={() => setAudioOut(previousState => !previousState)}
                                disabled={props.disabled || !recorder}/>
                    </div>
                    <div className="buttons">
                        <div className="blank"/>
                        <Button className="yellow medium-button" 
                            onClick={start}
                            disabled={props.disabled || !recorder}>
                            <i className="fas fa-microphone fa-3x"/>
                        </Button>
                        <div className="blank"/>
                    </div>
                    <h2><Timer milliseconds={timer.milliseconds}/></h2>
                    <canvas id={props.visualizerID} className="visualizer" width="300" height="150"></canvas>
                </div>
            );
    };
};

AudioRecorder.defaultProps = {
    visualizerID: uuidv4(),
    disabled: false,
};

export default AudioRecorder;