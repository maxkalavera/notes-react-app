import React from 'react';

function Timer(props) {
    const time = new Date(0, 0, 0, 0, 0, 0, props.milliseconds);
    const elements = [];
    if (props.useHours) elements.push(time.getHours());
    if (props.useMinutes) elements.push(time.getMinutes());
    if (props.useSeconds) elements.push(time.getSeconds());

    return (
        <React.Fragment>
            {
                elements.map((item, index) => {
                    if (index === elements.length - 1)
                        return `${item.toLocaleString(
                            process.env.LOCALE, {useGrouping: false, minimumIntegerDigits: 2})}`
                    return `${item.toLocaleString(
                        process.env.LOCALE, {useGrouping: false, minimumIntegerDigits: 2})}:`
                })
            }
        </React.Fragment>
    );
};

Timer.defaultProps = {
    useHours: true,
    useMinutes: true,
    useSeconds: true,
    milliseconds: 0,
};

export default Timer;