import React from 'react';

import logo from '@/assets/images/logo-outline-blue.svg';

function Logo() {
    return (
        <img src={logo} className="logo" alt="logo" draggable="false"/>
    );
};

export default Logo;