import React from 'react';

import Button from '@/components/Button';
import Ribbon from '@/components/Ribbon';

function NotesTopicCard(props) {
    return (
        <div className={`notes topic-card ${props.selected ? 'selected' : ''}`} style={props.style}>
            {
                props.ribbon ? 
                    <Ribbon className="lg" color={props.ribbon}/> : 
                    null
            }

            <div className="content">
                <Button 
                    className="transparent options icon"
                    onClick={props.onItemOption}>
                    <i className="fas fa-ellipsis-h fa-2x"/>
                </Button>
                <div className="title">
                    <Button 
                        className="transparent" 
                        onClick={props.onItemSelected}><h3>{props.title}</h3></Button>
                </div>
                <h5>{`${props.notes} Notes`}</h5>
            </div>
        </div>
    );
};

NotesTopicCard.defaultProps = {
    selected: false,
    title: "",
    notes: 0,
    onItemSelected: null,
    onItemOption: null,
    style: "",
    ribbon: "",
};

export default NotesTopicCard;