import React, { useEffect } from 'react';

import { v4 as uuidv4 } from 'uuid';

function Checkbox(props) {
    const {indeterminate, checkboxID} = props;

    useEffect(() => {
        if (indeterminate) {
                document.getElementById(checkboxID)
                    .indeterminate = indeterminate;
            }
    }, [indeterminate, checkboxID]);

    return (
        <div className="checkbox">
            <input
                id={props.checkboxID}
                type="checkbox"
                checked={props.checked}
                onChange={props.onChange}/>
            {props.label ? <label>{props.label}</label> : null}
        </div>
    );
};

Checkbox.defaultProps = {
    label: "",
    checked: false,
    indeterminate: false,
    onChange: null,
    checkboxID: uuidv4()
};

export default Checkbox;