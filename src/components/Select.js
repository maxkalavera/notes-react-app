import React, {useState, useCallback} from 'react';

import Ribbon from '@/components/Ribbon';

/*
    <Select
      disabled={false}
      type="secondary"
      hint="Type of note"
      items={[
        {value: "0", text: "Option A", ribbon: "label-1"},
        {value: "1", text: "Option B", ribbon: "label-2"},
        {value: "2", text: "Option C"},
        {value: "3", text: "Option D"},
        {value: "4", text: "Option E"},
      ]}
      selected="0"
      onItemSelect={null}
      itemComponent={OptionWithRibbon}/>
*/

function Select(props) {
    const [focused, setFocused] = useState(false);
    const {onItemSelect} = props;

    var selected = props.items.find(item => item.value === props.selected);

    const onItemClick = useCallback((value) => {
        if (onItemSelect) 
            onItemSelect(value);
    }, [onItemSelect]);

    if (props.disabled === true) {
        return (
            <div className="input-row">
                {props.label ? <label>{props.label}</label> : null}
                <div className="input-block">
                    <div className={`select disabled ${props.className}`}
                        tabIndex={0}
                        onClick={() => props.disabled === true ? null : setFocused(!focused)}
                        onBlur={() => props.disabled === true ? null : setFocused(false) }>
                        <div className="select-input">
                            { selected ? 
                                <props.itemComponent {...selected}/> :
                                <props.itemComponent  {...{ className: "hint", text: props.hint }}/> 
                            }
                        </div>
                    </div>
                </div>
            </div>
        );
    }
    // hidden
    return (
        <div className="input-row">
            {props.label ? <label>{props.label}</label> : null}
            <div className="input-block">
                <div className={`select ${props.className}`}
                    tabIndex={0}
                    onClick={props.onClick ? props.onClick : () => setFocused(!focused)}
                    onBlur={props.onClick ? null : () => setFocused(false) }>
                    <div className="select-input">
                        { selected ? 
                            <props.itemComponent {...selected}/> :
                            <props.itemComponent  {...{ className: "hint", text: props.hint }}/> 
                        }
                    </div>
                    <div className={focused ? "option-list" : "hidden"}>
                        {props.items.map((item) => (
                            <props.itemComponent 
                                key={item.value} 
                                onClick={() => onItemClick(item.value)} 
                                {...item}/>
                        ))}
                    </div>
                </div>
            </div>
        </div>
    );
};

Select.defaultProps = {
    label: "",
    items: [],
    itemComponent: Option,
    disabled: false,
    type: "default",
    onClick: null,
    className: ""
};

export function Option(props) {
    return (
        <div className={`option ${props.className}`}
            value={props.value}
            onClick={props.onClick}>
            <p>{props.text}</p>
        </div>
    );
};

Option.defaultProps = {
    value: "",
    text: "",
    onClick: null,
    className: ""
};

export function OptionWithRibbon(props) {
    return (
        <div className={`option ${props.className}`}
            value={props.value}
            onClick={props.onClick}>
            <Ribbon className="sm" color={props.ribbon}/>
            <p>{props.text}</p>
        </div>
    );
};

OptionWithRibbon.defaultProps = {
    value: "",
    text: "",
    onclick: null,
    className: "",
    ribbon: ""
};

export default Select;