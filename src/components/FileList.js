import React from 'react';

import Button from '@/components/Button';

function FileList(props) {
    return (
        <div className="list">
            {
                props.items.map((item) => (
                    <div className="item" 
                        key={item.key}>
                        <i className="fas fa-file-upload fa-2x"/>
                        <h4 className="grow">{item.name} ({item.size})</h4>
                        <i />
                        <Button className="transparent"
                            onClick={props.onDownload}>
                            <i className="fas fa-file-download fa-2x"/>
                        </Button>                        
                        <Button className="transparent"
                            onDelete={props.onDelete}>
                            <i className="fas fa-trash fa-2x"/>
                        </Button>
                    </div>
                ))
            }
        </div>
    );
};

FileList.defaultProps = {
    items: [],
    onDelete: null,
    onDownload: null
};

export default FileList;

/*
        {
            key: 0,
            name: "example-of-a-file.pdf",
            size: "12.54 MB",
            uploading: false 
        },
        {
            key: 1,
            name: "example-of-document.doc",
            size: "33.21 MB",
            uploading: false 
        },
        {
            key: 2,
            name: "example-of-a-file.deb",
            size: "15.33 MB",
            uploading: false 
        },
        {
            key: 3,
            name: "example-of-a-file-loading.txt",
            size: "40.00 MB",
            uploading: false 
        },
*/