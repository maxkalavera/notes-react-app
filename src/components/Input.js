import React from 'react';

function Input(props) {
  return (
    <div className={`input-row ${props.className}`}>
      {props.label ? <label>{props.label}</label> : null}
      <div className="input-block">
        <input 
          {...{
            ...props,
            placeholder: props.placeholder,
            type: "text",
            className: `${props.error} ${props.className}`
          }} />
          { props.error ?
            <span className="input-error">{props.error}</span> : null }
      </div>
    </div>
  );
};

Input.defaultProps = {
  label: "",
  placeholder: "Placeholder",
  error: "",
  className: "",
};

export default Input;