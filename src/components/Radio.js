import React from 'react';

function Radio(props) {
    return (
        <input {...{
                ...props,
                type: "radio",
            }} />
    );
};

export default Radio;