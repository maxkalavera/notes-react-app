import React from 'react';

import Button from '@/components/Button';

function LinkList(props) {
    return (
        <div className="list">
            {
                props.items.map((item) => (
                    <div className="item" 
                        key={item.key}>
                        <i className="fas fa-link fa-2x"/>
                        <a href={item.link} className="grow">
                            <h4 >{item.link}</h4>
                        </a>
                        <Button className="transparent"
                            onClick={props.onDelete}>
                            <i className="fas fa-trash fa-2x"/>
                        </Button>
                    </div>
                ))
            }
        </div>
    );
};

LinkList.defaultProps = {
    items: [],
    onDelete: null
};

export default LinkList;

/*
        {
            key: 0,
            link: "http://example.com/"
        },
        {
            key: 1,
            link: "http://basketball.example.com/basket/airport#army"
        },        {
            key: 2,
            link: "https://www.example.com/bomb/bomb.html"
        },        {
            key: 3,
            link: "http://www.example.com/bird/bat?advertisement=animal&apparel=amusement"
        },        {
            key: 4,
            link: "http://berry.example.org/"
        },        {
            key: 5,
            link: "https://www.example.org/adjustment/beds.php"
        },
*/