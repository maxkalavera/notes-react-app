import React from 'react';

function NavigationBar(props) {
    const path = window.location.pathname;
    
    return (
        <ul className="navbar">
            {
                props.items.map(item => 
                    <li key={item.value}>
                        <a href={item.src}>
                            <h2 className={
                                `${item.src === path ? "underline" : ""}`}>
                                {item.text}
                            </h2>
                        </a>
                    </li>    
                )
            }
        </ul>
    );
};

NavigationBar.defaultProps = {
    items: [
        {value: "notes", src: "/notes", text: "Notes"},
        {value: "topics", src: "/topics", text: "Topics"},
    ],
    selected: "",
};

export default NavigationBar;