import React, { useCallback } from 'react';

import Ribbon from '@/components/Ribbon';
import Input from '@/components/Input';
import Select, { OptionWithRibbon } from '@/components/Select';

const calculateColorRatio = (baseColor="#405580") => {
    baseColor = baseColor.replaceAll("#", "");

    let ratio = [
        parseInt(baseColor.slice(0, 2), 16), 
        parseInt(baseColor.slice(2, 4), 16), 
        parseInt(baseColor.slice(4, 6), 16)
    ];
    let maxValue = Math.max(...ratio);
    ratio = ratio.map((value) => value / maxValue)
    return ratio
};

const calculate_color = (
        weight,
        ratio=calculateColorRatio("#405580"),
        minColorValue=60,
        maxColorValue=170) => {
    return "#" + ratio.map((value) =>
        Math.floor(
            (minColorValue + ((1 - weight) * (maxColorValue - minColorValue))) * value
        ).toString(16).toUpperCase()
    ).join("");
};

function TopicCard(props) {
    const {mode} = props;

    return (
        <div className="topic-card">
            {(useCallback(() => {
                switch (mode) {
                    case "edit":
                        return (
                            <div className="header edit">
                                <Input placeholder="Topic name" value={props.name}/>
                                <Select disabled={false}
                                    hint="Label color"
                                    selected="0"
                                    className="labels label-3"
                                    items={[
                                        { value: "0", text: "Label 1" },
                                        { value: "0", text: "Label 2" },
                                        { value: "0", text: "Label 3" },
                                        { value: "0", text: "Label 4" },
                                        { value: "0", text: "Label 5" },
                                        { value: "0", text: "Label 6" },
                                        { value: "0", text: "Label 7" },
                                        { value: "0", text: "Label 8" },
                                        { value: "0", text: "Label 9" },
                                        { value: "0", text: "Label 10" },
                                        { value: "0", text: "Label 11" },
                                        { value: "0", text: "Label 12" },
                                        { value: "0", text: "Label 13" }
                                    ]}
                                    onClick={() => {}}/>
                            </div>
                        );
                    default: 
                        return (
                            <div className="header">
                                {
                                    props.ribbon ? 
                                        <div style={{height: "100%"}}>
                                            <Ribbon className="lg" color={props.ribbon}/>
                                        </div> : null
                                }
                                {props.name ? <h3 className="grow">{props.name}</h3> : null}
                            </div>
                        );
                }
            }, [mode]))()}
            <div className="content">
                <h2>{props.notes} Notes</h2>
                <i className="fas fa-circle" 
                    style={{
                        fontSize: `${126 + (126 * props.weight)}px`,
                        color: calculate_color(props.weight)
                    }}/>
            </div>
        </div>
    );
};

TopicCard.defaultProps = {
    name: "General ideas",
    notes: 27,
    weight: 0.5,
    ribbon: "label-1",
    mode: "edit" // default | edit | disabled
};

export default TopicCard;