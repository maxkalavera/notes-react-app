import React, { useState, useCallback } from 'react';

import Button from '@/components/Button';
import Checkbox from '@/components/Checkbox';

function NotesTable(props) {
    const [selectedItems, setSelectedItems] = useState({});

    const getTypeName = useCallback((type) => {
        switch (type) {
            case "text":
                return "Text";
            case "file":
                return "File";
            case "image":
                return "Image";
            case "video":
                return "Video";
            case "links":
                return "Links";
            case "audio":
                return "Audio";
            default:
                return "";
        }
    }, []);

    const selectItem = (item) => {
        setSelectedItems(prevState => ({
            ...prevState,
            [item.key]: item
        }));
    };

    const removeItem = (item) => setSelectedItems(prevState => {
        let items = Object.assign({}, prevState);
        delete items[item.key];
        return items;
    });

    return (
        <table>
            <thead>
                <tr>
                    { props.mode === 'selection' ? 
                        <th>
                            
                        </th> 
                        : null    
                    }
                    <th><h3>Name</h3></th>
                    <th><h3>Topic</h3></th>
                    <th><h3>Type</h3></th>
                    <th><h3>Date</h3></th>
                    <th className="center"><h3>Action</h3></th>
                </tr>
            </thead>
            <tbody>
                {
                    props.items.map((item) => {
                        let isSelected = selectedItems.hasOwnProperty(item.key);

                        return (
                            <tr key={item.key} className={`${isSelected ? "selected" : ""}`}>
                                { props.mode === 'selection' ?
                                    <td>
                                        <Checkbox 
                                            checked={isSelected}
                                            onChange={() => 
                                                isSelected ? removeItem(item) :
                                                    selectItem(item)
                                            }/>
                                    </td>
                                    : null
                                }
                                <td><p>{item.name}</p></td>
                                <td><p>{item.topic}</p></td>
                                <td><p>{getTypeName(item.type)}</p></td>
                                <td>
                                    <p>
                                        {new Intl.DateTimeFormat(process.env.LOCALE, {
                                            year: "numeric", month: "numeric", day: "2-digit",
                                            hour: 'numeric', minute: 'numeric'
                                        }).format(item.date)}
                                    </p>
                                </td>
                                <td className="center">
                                    <Button className="transparent">
                                        <i className="fas fa-ellipsis-h"/>
                                    </Button>

                                </td>
                            </tr>
                        );
                    })
                }
            </tbody>
            {
                props.loadMore ? (
                    <tfoot>
                        <tr>
                            <td colspan="6" align="center">
                                <Button 
                                    onClick={props.loadMore} 
                                    className="link">
                                    <h4>Load More</h4>
                                </Button>
                            </td>
                        </tr>
                    </tfoot>                             
                ) : null
            }
        </table>
    );
};

NotesTable.defaultProps = {
    loadMore: null,
    items: [],
    mode: "default" | "selection"
};

export default NotesTable;

/*
        {
            key: 0,
            name: "Lorep ipsum",
            topic: "General",
            type: "text",
            date: new Date(2021, 6, 5, 11, 59)
        },
        {
            key: 1,
            name: "File-name.pdf",
            topic: "General",
            type: "file",
            date: new Date(2021, 6, 5, 4, 0)
        },
        {
            key: 2,
            name: "Breakfast.jpg",
            topic: "General",
            type: "image",
            date: new Date(2021, 6, 5, 5, 22)
        },
        {
            key: 3,
            name: "People-is-moving.wav",
            topic: "General",
            type: "video",
            date: new Date(2021, 6, 5, 10, 33)
        },
        {
            key: 4,
            name: "Links of design",
            topic: "General",
            type: "links",
            date: new Date(2021, 6, 5, 12, 34)
        },
        {
            key: 5,
            name: "Audio records 01",
            topic: "General",
            type: "audio",
            date: new Date(2021, 6, 5, 6, 43)
        },
*/