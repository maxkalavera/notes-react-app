import React from 'react';

import '@styles/main.scss';

//import Button from '@/components/Button';
//import Input from '@/components/Input';
//import Checkbox from '@/components/Checkbox';
//import Switch from '@/components/Switch';
//import Radio from '@/components/Radio';
//import Alert from '@/components/Alert';
//import Ribbon from '@/components/Ribbon';
//import Select, { OptionWithRibbon } from '@/components/Select';
//import Avatar from '@/components/Avatar';
//import NavigationBar from '@/components/NavigationBar';

//import NotesCard from '@/components/NotesCard';
//import FileUploader from '@/components/FileUploader';
//import AudioRecorder from '@/components/AudioRecorder';
//import AudioPlayer from '@/components/AudioPlayer';
//import NotesTable from '@/components/NotesTable';
//import AudioPlaylist from '@/components/AudioPlaylist';
//import LinkList from '@/components/LinkList';
//import FileList from '@/components/FileList';
//import LinkForm from '@/components/LinkForm';
//import TopicCard from '@/components/TopicCard';

//import Header from '@/layout/Header';
//import TitleMenu from '@/layout/TitleMenu';
//import NotesTopics from '@/layout/NotesTopics';
//import LastNotes from '@/layout/LastNotes';

//import Login from '@/features/Login';

function App() {
    return (
        <div style={{display: "flex", flexDirection: "column", justifyContent: "center", alignItems: "center", height: "100vh", gap: "12px"}}>
            <TopicCard />
        </div>
    );
};

export default App;
