import React from 'react';

import Button from '@/components/Button';
import Select, { OptionWithRibbon } from '@/components/Select';
import Input from '@/components/Input';

function TitleMenu(props) {

    switch(props.view) {
        case "notes": return (
            <div id="title-menu">
                <h2 className="grow">Notes</h2>
                <Button className="blue"><h4>New note</h4></Button>
            </div>
        );
        case "topics": return (
            <div id="title-menu">
                <h2 className="grow">Topics</h2>
                <Button className="blue"><h4>New topic</h4></Button>
            </div>
        );
        case "new-note": return (
            <div id="title-menu">
                <div className="grow"></div>
                <Select
                    disabled={false}
                    label="Topic"
                    className="secondary"
                    items={[
                        {value: "general", text: "General"},
                        {value: "0", text: "Option A", ribbonClassName: "label-1"},
                        {value: "1", text: "Option B", ribbonClassName: "label-2"},
                        {value: "2", text: "Option C"},
                        {value: "3", text: "Option D"},
                        {value: "4", text: "Option E"},
                    ]}
                    selected="general"
                    onItemSelect={null}
                    itemComponent={OptionWithRibbon}/>
            </div>
        );
        case "new-note-with-name": return (
            <div id="title-menu">
                <Input className="grow" placeholder="Note name (Optional)"/>
                <Select
                    disabled={false}
                    label="Topic"
                    className="secondary"
                    items={[
                        {value: "general", text: "General"},
                        {value: "0", text: "Option A", ribbonClassName: "label-1"},
                        {value: "1", text: "Option B", ribbonClassName: "label-2"},
                        {value: "2", text: "Option C"},
                        {value: "3", text: "Option D"},
                        {value: "4", text: "Option E"},
                    ]}
                    selected="general"
                    onItemSelect={null}
                    itemComponent={OptionWithRibbon}/>
            </div>
        );
        case "new-topic": return (
            <div id="title-menu">
                <h2 className="grow">New topic</h2>
                <Button className="blue"><h4>Save</h4></Button>
                <Button className="secondary blue"><h4>Cancel</h4></Button>
            </div>
        );
        case "view-topic": return (
            <div id="title-menu">
                <Button className="transparent icon"><i className="fas fa-arrow-left fa-2x text-shadow" /></Button>
                <div className="grow"/>
                <Button className="blue"><h4>Edit</h4></Button>
            </div>
        );
        case "edit-topic": return (
            <div id="title-menu">
                <Button className="transparent icon"><i className="fas fa-arrow-left fa-2x" /></Button>
                <div className="grow"/>
                <Button className="blue"><h4>Done</h4></Button>
            </div>
        );
        default: return (
            <div id="title-menu">
            </div>    
        );
    }
};

TitleMenu.defaultProps = {
    view: "edit-topic"
};

export default TitleMenu;