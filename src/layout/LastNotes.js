import React from 'react';

import LastNotesCard from '@/components/LastNotesCard';

function LastNotes(props) {

    return (
        <div id="last-notes">
            <div className="title">
                <h3>Last notes added</h3>
            </div>
            <div className="list">
                {
                    props.items.map((item) =>
                        <LastNotesCard {...item}/>
                    )
                }
            </div>
        </div>
    );
};

LastNotes.defaultProps = {
    items: []
};

export default LastNotes;