import React from 'react';

import Button from '@/components/Button';
import NotesTopicCard from '@/components/NotesTopicCard';

/*
      <TopicCardList
        items={[
          {
            key: "0",
            title: "General ideas",
            notes: 27,
          },
          {
              key: "1",
              title: "Nemo Enim",
              notes: 32,
          },
          {
              key: "2",
              title: "Vitae",
              notes: 3,
          },
          {
              key: "3",
              title: "Voluptate velit",
              notes: 0,
          },
        ]}/>
*/

function NotesTopicCardList(props) {
    return (
        <div id="topic-card-list" className="notes">
            {
                props.items.map((item, index) => (
                    <NotesTopicCard {...{
                        ...item,
                        // If is the selected card put in on Top of the others
                        style: {zIndex: 2147483647 - (props.selected === item.key ? 0 : 1 + index)},
                        selected: props.selected === item.key,
                        onItemSelected: () => {props.onItemSelected(item.key)},
                        onItemOption: () => {props.onItemOption(item.key)}
                        }}/>
                ))
            }
            {
                props.loadMore ? (
                    <Button 
                        onClick={props.loadMore} 
                        className="link">
                        <h4>Load More</h4>
                    </Button>
                ) : null
            }
        </div>
    );
};

NotesTopicCardList.defaultProps = {
    selected: null,
    items: [],
    onItemSelected: ()=>{},
    onItemOption: ()=>{},
    loadMore: null
};

export default NotesTopicCardList;