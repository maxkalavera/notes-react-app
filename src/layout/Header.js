import React from 'react';

import Logo from '@/components/Logo';
import Avatar from '@/components/Avatar';
import NavigationBar from '@/components/NavigationBar';

function Header(props) {
    return (
        <div id="header">
            <div className="content">
                <a href="/">
                    <Logo />
                </a>
                <NavigationBar />
                <div className="user-menu">
                    <Avatar src={props.src} />
                    <h3>{props.username}</h3>
                </div>
            </div>
        </div>
    );
};

Header.defaultProps = {
    avatarSrc: "",
    username: "Username"
};

export default Header;