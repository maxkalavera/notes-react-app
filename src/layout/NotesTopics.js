import React, { useState, useCallback } from 'react';

import Button from '@/components/Button';
import NotesTopicCardList from '@/layout/NotesTopicCardList';

function NotesTopics(props) {
    let [selected, setItemSelected] =  useState("0");

    const onAddTopic = useCallback(() => {

    }, []);

    return (
        <div className="notes topics">
            <div className="header">
                <h3>Topics</h3>
                <Button 
                    className="transparent"
                    onClick={onAddTopic}>
                    <i className="fas fa-plus-square fa-2x"/>
                </Button>
            </div>
            <NotesTopicCardList
                selected={selected}
                onItemSelected={setItemSelected}
                items={props.items}/>
        </div>
    );
};

NotesTopics.defaultProps = {
    items: [
        {
            key: "0",
            title: "General ideas",
            notes: 27,
        },
        {
            key: "1",
            title: "Nemo Enim",
            notes: 32,
        },
        {
            key: "2",
            title: "Vitae",
            notes: 3,
        },
        {
            key: "3",
            title: "Voluptate velit",
            notes: 0,
            ribbon: "label-1"
        },
    ]
};

export default NotesTopics;