import React from 'react';

import Button from '@/components/Button';
import Input from '@/components/Input';
import Checkbox from '@/components/Checkbox';

function Login(props) {
    return (
        <div id="login">
            <div className="background-image" />
            <div className="login-container">
                <div id="login-form">
                    <div>
                        <h1>Notes by Max</h1>
                        <h3>An app to handfully record your ideas</h3>
                    </div>

                    <form >
                        <Input placeholder="Username" />
                        <Input placeholder="Password" type="password" />
                        <Checkbox label="Remember me"/>
                        <Button>Sign in</Button>
                    </form>
                </div>
            </div>
        </div>
    );
};

export default Login;