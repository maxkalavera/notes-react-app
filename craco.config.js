const path = require('path');
const webpack = require('webpack');

module.exports = {
    webpack: {
        alias: {
            '@': path.resolve(__dirname, 'src/'),
            '@styles': path.resolve(__dirname, 'src/styles/'),
        },
        plugins: {
            add: [
                new webpack.EnvironmentPlugin({
                    NODE_ENV: 'development', // use 'development' unless process.env.NODE_ENV is defined
                    DEBUG: false,
                    SASS_PATH: 'node_modules:styles',
                    LOCALE: 'en-US'
                  })
            ]
        }
    }
};